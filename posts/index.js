const express = require("express");
const bodyParser = require("body-parser");
const { randomBytes } = require("crypto");
const cors = require("cors");
const axios = require("axios");

const app = express();
app.use(bodyParser.json());
app.use(cors());

const posts = {};

// this endpoint is obsolete, left for debug reason
app.get("/posts", (req, res) => {
  res.send(posts);
});

app.post("/posts/create", async (req, res) => {
  const id = randomBytes(4).toString("hex");
  const { title } = req.body;

  posts[id] = {
    id,
    title,
  };

  const event = { type: "PostCreated", data: { id, title } };
  await axios.post("http://event-bus-clusterip-srv:4005/events", event);

  res.status(201).send(posts[id]);
});

app.post("/events", (req, resp) => {
  console.log("Event caught: ", req.body);
  resp.send({});
});

app.listen(4000, () => {
  console.log("Listening on 4000");
});
