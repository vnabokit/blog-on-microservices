const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const axios = require("axios");

const app = express();
app.use(bodyParser.json());
app.use(cors());

let postsComments = {};

const handleEvents = (type, data) => {
  if (type == "PostCreated") {
    postsComments[data.id] = {
      ...data,
      comments: [],
    };
  }

  if (type == "CommentCreated") {
    const { id, content, postId, status } = data;
    const post = postsComments[postId];
    post.comments.push({
      id,
      content,
      status,
    });
  }

  if (type == "CommentUpdated") {
    const { id, content, postId, status } = data;
    const post = postsComments[postId];
    const comment = post.comments.find((comment) => {
      return comment.id === id;
    });
    comment.content = content;
    comment.status = status;
  }
};

app.get("/posts", (req, resp) => {
  resp.send(postsComments);
});

app.post("/events", (req, resp) => {
  const { type, data } = req.body;
  handleEvents(type, data);
  resp.send({});
});

app.listen(4002, async () => {
  console.log("Query service listens on 4002");
  const res = await axios.get("http://event-bus-clusterip-srv:4005/events");
  for (let event of res.data) {
    console.log("Processing event: " + event.type);
    handleEvents(event.type, event.data);
  }
});
