## General info

This project realizes simple blog. The key feature is that this blog is built fully using microservices approach.

## Technologies

Project is created with:

- Node.js
- Express.js
- Docker
- Kubernetes, Skaffold

## This project can:

- Create new post
- Add a comment to a post

## About microservices architecture there

### Responsibilities

Eeach nicroservice is placed to a separate folder:

- `./posts`. Responsible for operations with posts (create, get)
- `./comments`. Responsible for operations with comments (create, get)
- `./query`. Responsible for getting info about all posts and their comments by a single HTTP-request
- `./moderation`. Responsible for moderation a comment
- `./event-bus`. Simple event bus prototype (written by hands just to understand principles of this tool)

## Setup

### Install the project locally

```
$ git clone https://gitlab.com/vnabokit/blog-on-microservices.git
```
